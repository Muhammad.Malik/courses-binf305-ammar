clc;clear all; close all;
%parameters
p=zeros(1,4);
p(1)=0.05;        %k1
p(2)=0.7;         %k-1
p(3)=0.005;         %k2
p(4)=0.4;       %k-2

tspan=[0,20];

y0 = [1.5 3 2];

[T,Y] = ode45(@testfun1,tspan,y0,[],p);
figure(1)
plot(T,Y(:,1),'linewidth',2);    %A
hold on
plot(T,Y(:,2),'linewidth',2);   %B
plot(T,Y(:,3),'linewidth',2);   %C
xlabel('time')
ylabel('Production')
%legend('A','B','C');

% figure(2)
% plot(T(1:20),Y(1:20,1),'linewidth',2);    %A
% hold on
% plot(T(1:20),Y(1:20,2),'linewidth',2);   %B
% plot(T(1:20),Y(1:20,3),'linewidth',2);   %C
% xlabel('time')
% ylabel('Production')
% legend('A','B','C');

[T2,Y2] = ode45(@testfun3,tspan,[1.5 2],[],p);
%figure(1)
plot(T2,Y2(:,1),'kx');    %A
hold on
%plot(T,Y(:,2),'linewidth',2);   %B
plot(T2,Y2(:,2),'rx');   %C
xlabel('time')
ylabel('Production')
legend('A','B','C','A-qssa','C-qssa');

 Sqssa=interp1(T2,Y2,T);
 figure(2)
%plot(T,Y(:,1)-Sqssa(:,1))
% %hold on
 plot(T,Y(:,3)-Sqssa(:,2))
% ylabel('S-S_{qssa}')
 legend('difference between correct solution and QSSA')

function f=testfun1(t,y,p)
f=zeros(3,1);
f(1) = -p(1)*y(1) + p(2)*y(2);                
f(2) = p(1)*y(1) - p(2)*y(2) - p(3)*y(2) + p(4)*y(3);                   
f(3) = p(3)*y(2) - p(4)*y(3); 
end

function f=testfun3(t,y,p)
f=zeros(2,1);
f(1) = -p(1)*y(1) + (p(2)*((p(1)*y(1))+(p(4)*y(2)))/(p(2)+p(3)));                                 
f(2) = (p(3)*((p(1)*y(1))+(p(4)*y(2)))/(p(2)+p(3))) - p(4)*y(2); 
end

