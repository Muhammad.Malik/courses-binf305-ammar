clc;clear all;close all;
k = [1.34, 1.6e9, 8e3, 4e7, 1];
y0 = [0.6e-1, 0.33e-6, 0.501e-10, 0.3e-1, 0.24e-7];

Res = @(t,y)[(-k(1)*y(1)*y(2))+(-k(3)*y(1)*y(3));
             (-k(1)*y(1)*y(2))+(-k(2)*y(2)*y(3))+(k(5)*y(5));
             (k(1)*y(1)*y(2))+(-k(2)*y(2)*y(3))+(k(3)*y(1)*y(3))+(-2*k(4)*y(3)*y(3));
             (k(2)*y(2)*y(3))+ (k(4)*y(3)*y(3));
             (k(3)*y(1)*y(3))+(-k(5)*y(5))];
%options=odeset('InitialStep',1e-5,'MaxStep',0.2,'reltol',1e-8,'abstol',1e-10);
options=odeset('reltol',1e-8,'abstol',1e-10);
timeinterval=[0.0, 200.0];
[t,y] = ode15s(Res, timeinterval, y0, options) ; %implicit solver for stiff systems
figure;
y = log(y);
plot(t,y(:,1), 'linewidth',2)
hold on
plot(t,y(:,2), 'linewidth',2)
plot(t,y(:,3), 'linewidth',2)
plot(t,y(:,4), 'linewidth',2)
plot(t,y(:,5), 'linewidth',2)
xlabel('time')
ylabel('Production')
legend('A','B','C','D','E');